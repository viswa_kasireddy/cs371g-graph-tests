// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);

}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, bidirectional1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    // Adjacent to vdA
    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(e, b);

    // Adjacent to vdB
    pair<adjacency_iterator, adjacency_iterator> p1 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b1 = p1.first;
    adjacency_iterator                           e1 = p1.second;
    ASSERT_NE(b1, e1);

    vertex_descriptor vd11 = *b1;
    ASSERT_EQ(vd11, vdA);
    ++b1;
    ASSERT_EQ(e1, b1);
}

TYPED_TEST(GraphFixture, add_same_edge) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> x = add_edge(vdA, vdB, g);
    ASSERT_EQ(x.second, false);

    // Adjacent to vdA
    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, edge_source) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> x1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> x2 = add_edge(vdB, vdA, g);

    vertex_descriptor s1 = source(x1.first, g);
    vertex_descriptor s2 = source(x2.first, g);

    ASSERT_EQ(s1, vdA);
    ASSERT_EQ(s2, vdB);
}

TYPED_TEST(GraphFixture, edge_target) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> x1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> x2 = add_edge(vdB, vdA, g);

    vertex_descriptor s1 = target(x1.first, g);
    vertex_descriptor s2 = target(x2.first, g);

    ASSERT_EQ(s1, vdB);
    ASSERT_EQ(s2, vdA);
}

TYPED_TEST(GraphFixture, num_edges) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    add_edge(vdA, vdB, g);
    ASSERT_EQ(1, num_edges(g));
    add_edge(vdA, vdB, g);
    ASSERT_EQ(1, num_edges(g));
}

TYPED_TEST(GraphFixture, num_vertices) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_EQ(0, num_vertices(g));
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(2, num_vertices(g));
    add_vertex(g);
    add_vertex(g);
    ASSERT_EQ(4, num_vertices(g));
}

TYPED_TEST(GraphFixture, two_graphs_source) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g1);

    std::pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g1);

    ASSERT_EQ(source(p1.first, g2), vdA);
}

TYPED_TEST(GraphFixture, two_graphs_target) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g1);
    std::pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g1);

    ASSERT_EQ(target(p1.first, g2), vdB);
}

TYPED_TEST(GraphFixture, adding_across_graphs) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    std::pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g1);
    std::pair<edge_descriptor, bool> p2 = add_edge(vdB, vdA, g2);

    ASSERT_EQ(p1.second, true);
    ASSERT_EQ(p2.second, true);
}

TYPED_TEST(GraphFixture, adding_across_graphs_target) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    std::pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g1);
    std::pair<edge_descriptor, bool> p2 = add_edge(vdB, vdA, g2);

    ASSERT_EQ(target(p1.first, g1), vdB);
    ASSERT_EQ(target(p1.first, g2), vdB);
    ASSERT_EQ(target(p2.first, g1), vdA);
    ASSERT_EQ(target(p2.first, g2), vdA);
}

TYPED_TEST(GraphFixture, adding_across_graphs_source) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    std::pair<edge_descriptor, bool> p1 = add_edge(vdB, vdA, g1);
    std::pair<edge_descriptor, bool> p2 = add_edge(vdA, vdB, g2);

    ASSERT_EQ(source(p1.first, g1), vdB);
    ASSERT_EQ(source(p1.first, g2), vdB);
    ASSERT_EQ(source(p2.first, g1), vdA);
    ASSERT_EQ(source(p2.first, g2), vdA);
}

TYPED_TEST(GraphFixture, adding_across_graphs_iterator) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    add_edge(vdB, vdA, g1);
    add_edge(vdA, vdB, g2);

    pair<adjacency_iterator, adjacency_iterator> it1 = adjacent_vertices(vdB, g1);

    ASSERT_EQ(*(it1.first), vdA);
    ASSERT_EQ(*(it1.first), vdB);
}

TYPED_TEST(GraphFixture, adding_across_graphs_vertex_descriptor) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    ASSERT_EQ(vdA, vdB);
}

TYPED_TEST(GraphFixture, adding_across_graphs_num_edges) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g1);
    vertex_descriptor vdB = add_vertex(g2);
    add_edge(vdA, vdB, g1);
    add_edge(vdA, vdB, g2);
    add_edge(vdB, vdA, g2);

    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_edges(g2), 1);
}

TYPED_TEST(GraphFixture, bidirectional_edges) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    ASSERT_EQ(num_edges(g), 2);
}

TYPED_TEST(GraphFixture, two_graphs_num_vertices) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g1;
    graph_type g2;

    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);

    ASSERT_EQ(num_vertices(g2), 2);
}

TYPED_TEST(GraphFixture, num_edges_0) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    add_vertex(g);

    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, empty_vertex_iterator) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    pair<vertex_iterator, vertex_iterator> it = vertices(g);
    ASSERT_EQ(it.first, it.second);
}

TYPED_TEST(GraphFixture, empty_edge_iterator) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    pair<edge_iterator, edge_iterator> it = edges(g);
    ASSERT_EQ(it.first, it.second);
}

TYPED_TEST(GraphFixture, empty_adjacency_iterator) {
    using graph_type             = typename TestFixture::graph_type;
    using vertex_descriptor      = typename TestFixture::vertex_descriptor;
    using adjacency_iterator     = typename TestFixture::adjacency_iterator;

    graph_type g;
    vertex_descriptor v = add_vertex(g);
    pair<adjacency_iterator, adjacency_iterator> it = adjacent_vertices(v, g);
    ASSERT_EQ(it.first, it.second);
}

TYPED_TEST(GraphFixture, edge_test) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g);
    ASSERT_EQ(edge(vdA, vdB, g), p);
}

TYPED_TEST(GraphFixture, edge_fail_test) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g);
    ASSERT_NE(edge(vdB, vdA, g), p);
}

TYPED_TEST(GraphFixture, num_edges_test) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    for(int i = 0; i < 15; ++i)
        add_edge(vdA, vdB, g);
    ASSERT_EQ(num_edges(g), 1);
}

TYPED_TEST(GraphFixture, edges_test) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    for(int i = 0; i < 10; ++i)
        edge(vdA, vdB, g);
    for(int i = 0; i < 10; ++i)
        edge(vdB, vdA, g);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, edge_between_one_vertex) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<edge_descriptor, bool> p = add_edge(vdA, vdA, g);

    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 1);
    ASSERT_EQ(p.second, true);
}

TYPED_TEST(GraphFixture, add_nonexistent_vertex_edge) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    add_vertex(g);

    pair<edge_descriptor, bool> p = add_edge(vdA, 5, g);

    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 6);
    ASSERT_EQ(p.second, true);
}

TYPED_TEST(GraphFixture, add_nonexistent_vertex_edge_two_graphs) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    add_vertex(g1);
    vertex_descriptor vdA = add_vertex(g1);
    for (int i = 0; i < 10; i++) {
        add_vertex(g2);
    }
    vertex_descriptor vdB = add_vertex(g2);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g1);

    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_vertices(g1), 11);
    ASSERT_EQ(p.second, true);
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(num_vertices(g2), 11);
}

TYPED_TEST(GraphFixture, add_nonexistent_vertex_edge_two_graphs2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA = add_vertex(g2);
    vertex_descriptor vdB = add_vertex(g2);
    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g1);

    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_vertices(g1), 2);
    ASSERT_EQ(p.second, true);
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(num_vertices(g2), 2);
}