// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream>       // cout, endl
#include <iterator>       // ostream_iterator
#include <sstream>        // ostringstream
#include <utility>        // pair
#include <vector>         // vector
#include <unordered_set>  // unordered_set

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

// ------------
// vertex tests
// ------------

TYPED_TEST(GraphFixture, vertex_test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vd = vertex(0, g);

    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, vertex_test1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vd = vertex(0, g);

    ASSERT_EQ(vd, vdA);

    vertex_descriptor vdB = add_vertex(g);
    vd = vertex(1, g);

    ASSERT_EQ(vd, vdB);

    ASSERT_NE(vdA, vdB);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 2u);
}

TYPED_TEST(GraphFixture, vertex_test2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for (size_t i = 0; i < 1000000; ++i) {
        vertex_descriptor vdA = add_vertex(g);
        vertex_descriptor vd = vertex(i, g);
        ASSERT_EQ(vd, vdA);
    }

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1000000u);
}

// -----------
// empty tests
// -----------

TYPED_TEST(GraphFixture, empty_vertex_graph0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 0u);
}

TYPED_TEST(GraphFixture, empty_vertex_graph1) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, empty_edge_graph0) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type    = typename TestFixture::edges_size_type;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 0u);
}

TYPED_TEST(GraphFixture, empty_edge_graph1) {
    using graph_type         = typename TestFixture::graph_type;
    using edge_iterator   = typename TestFixture::edge_iterator;

    graph_type g;

    add_vertex(g);
    add_vertex(g);

    pair<edge_iterator, edge_iterator>   p = edges(g);
    edge_iterator                        b = p.first;
    edge_iterator                        e = p.second;
    ASSERT_EQ(b, e);
}

// ----------
// size tests
// ----------

TYPED_TEST(GraphFixture, vertex_size_test) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for (int i = 0; i < 10; ++i) {
        add_vertex(g);
    }

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 10u);
}

TYPED_TEST(GraphFixture, edge_size_test) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edges_size_type    = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 2u);
}

// ----------
// edge tests
// ----------

TYPED_TEST(GraphFixture, edge_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g);
    ASSERT_TRUE(p.second);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_TRUE(p2.second);

    ASSERT_EQ(p.first, p2.first);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}


TYPED_TEST(GraphFixture, edge_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}

TYPED_TEST(GraphFixture, edge_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    ASSERT_NE(edAB, edBA);

    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, true);

    pair<edge_descriptor, bool> p2 = edge(vdB, vdA, g);
    ASSERT_EQ(p2.first,  edBA);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 2u);
}

TYPED_TEST(GraphFixture, edge_test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    // A --> B
    // A --> C
    // C --> A
    // C --> B
    // B --> A
    // B --> C
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    ASSERT_NE(edAB, edBA);
    ASSERT_NE(edAC, edCA);
    ASSERT_NE(edBC, edCB);
}

TYPED_TEST(GraphFixture, edge_test4) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = edge(vdA, vdB, g);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdB, vdA, g);
    ASSERT_EQ(p2.second, false);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 0u);
}

TYPED_TEST(GraphFixture, edge_test5) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    edge_descriptor edAB = edge(vdA, vdB, g).first;
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, true);

    pair<edge_descriptor, bool> p2 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, false);
}

TYPED_TEST(GraphFixture, edge_test6) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    for (int i = 0; i < 10; ++i) {
        add_edge(vdA, vdB, g);
    }

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}

// ------------
// source tests
// ------------

TYPED_TEST(GraphFixture, source_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    vertex_descriptor s = source(edAB, g);

    ASSERT_EQ(vdA, s);
}

TYPED_TEST(GraphFixture, source_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    vertex_descriptor sA = source(edAB, g);
    vertex_descriptor sB = source(edBA, g);

    ASSERT_NE(sA, sB);
    ASSERT_EQ(vdA, sA);
    ASSERT_EQ(vdB, sB);
}

TYPED_TEST(GraphFixture, source_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    // A --> B
    // A --> C
    // C --> A
    // C --> B
    // B --> A
    // B --> C
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    ASSERT_EQ(vdA, source(edAB, g));
    ASSERT_EQ(vdA, source(edAC, g));
    ASSERT_EQ(vdB, source(edBA, g));
    ASSERT_EQ(vdB, source(edBC, g));
    ASSERT_EQ(vdC, source(edCA, g));
    ASSERT_EQ(vdC, source(edCB, g));
}

// ------------
// target tests
// ------------

TYPED_TEST(GraphFixture, target_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    vertex_descriptor t = target(edAB, g);

    ASSERT_EQ(vdB, t);
}

TYPED_TEST(GraphFixture, target_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    vertex_descriptor tB = target(edAB, g);
    vertex_descriptor tA = target(edBA, g);

    ASSERT_NE(tA, tB);
    ASSERT_EQ(vdB, tB);
    ASSERT_EQ(vdA, tA);
}

TYPED_TEST(GraphFixture, target_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    // A --> B
    // A --> C
    // C --> A
    // C --> B
    // B --> A
    // B --> C
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;

    ASSERT_EQ(vdB, target(edAB, g));
    ASSERT_EQ(vdC, target(edAC, g));
    ASSERT_EQ(vdA, target(edBA, g));
    ASSERT_EQ(vdC, target(edBC, g));
    ASSERT_EQ(vdA, target(edCA, g));
    ASSERT_EQ(vdB, target(edCB, g));
}

// ---------------------
// vertex iterator tests
// ---------------------

TYPED_TEST(GraphFixture, vertex_iterator_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, vertex_iterator_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    unordered_set<vertex_descriptor> expected_vds = {vdA, vdB};
    unordered_set<vertex_descriptor> vds;
    while (b != e) {
        vds.insert({*b});
        ++b;
    }

    ASSERT_EQ(expected_vds, vds);
}

TYPED_TEST(GraphFixture, vertex_iterator_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    unordered_set<vertex_descriptor> expected_vds;
    for (size_t i = 0; i < 1000000; ++i) {
        vertex_descriptor vd = add_vertex(g);
        expected_vds.insert({vd});
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    unordered_set<vertex_descriptor> vds;
    while (b != e) {
        vds.insert({*b});
        ++b;
    }

    ASSERT_EQ(expected_vds, vds);
}

// -------------------
// edge iterator tests
// -------------------

TYPED_TEST(GraphFixture, edge_iterator_test0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edge_iterator_test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);
    ASSERT_EQ(++++b, e);
}

TYPED_TEST(GraphFixture, edge_iterator_test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);
    ++++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edge_iterator_test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    // A --> B
    // A --> C
    // C --> A
    // C --> B
    // B --> A
    // B --> C
    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdB, vdC, g);

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);
    ++++++++++++b;
    ASSERT_EQ(b, e);
}

// ------------------------
// adjacency iterator tests
// ------------------------

TYPED_TEST(GraphFixture, adjacency_iterator_test0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, adjacency_iterator_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_EQ(b, e);

    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    adjacency_iterator                           b2 = p2.first;
    adjacency_iterator                           e2 = p2.second;
    ASSERT_NE(b2, e2);

    vertex_descriptor vd2 = *b2;
    ASSERT_EQ(vd2, vdA);
    ++b2;
    ASSERT_EQ(b2, e2);
}

TYPED_TEST(GraphFixture, adjacency_iterator_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    unordered_set<vertex_descriptor> expected({1, 2});
    unordered_set<vertex_descriptor> actual;

    vertex_descriptor vd1 = *b;
    actual.insert(vd1);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    actual.insert(vd2);
    ++b;
    ASSERT_EQ(e, b);

    ASSERT_EQ(expected, actual);
}

// ---------------
// edge case tests
// ---------------

TYPED_TEST(GraphFixture, edge_case_test0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = vertex(0, g);
    vertex_descriptor vdB = vertex(1, g);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 0u);

    edge_size_type es = num_edges(g);
    ASSERT_EQ(es, 0u);

    pair<edge_descriptor, bool> p = add_edge(vdA, vdB, g);
    edge_descriptor edAB = edge(vdA, vdB, g).first;

    ASSERT_TRUE(p.second);
    ASSERT_EQ(edAB, p.first);

    vertices_size_type vs2 = num_vertices(g);
    ASSERT_EQ(vs2, 2u);

    edge_size_type es2 = num_edges(g);
    ASSERT_EQ(es2, 1u);
}

TYPED_TEST(GraphFixture, edge_case_test1) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 0u);

    edge_size_type es = num_edges(g);
    ASSERT_EQ(es, 0u);

    pair<edge_descriptor, bool> edge_pair = add_edge(10, 12, g);

    ASSERT_TRUE(edge_pair.second);

    vertices_size_type vs2 = num_vertices(g);
    ASSERT_EQ(vs2, 13u);

    edge_size_type es2 = num_edges(g);
    ASSERT_EQ(es2, 1u);
}

TYPED_TEST(GraphFixture, edge_case_test2) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> edge_pair = edge(vdB, vdC, g);

    ASSERT_TRUE(!(edge_pair.second));

    vertex_descriptor vdS = source(edge_pair.first, g);

    ASSERT_EQ(vdB, vdS);
}

TYPED_TEST(GraphFixture, edge_case_test3) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> edge_pair = edge(vdB, vdC, g);

    ASSERT_TRUE(!(edge_pair.second));

    vertex_descriptor vdS = target(edge_pair.first, g);

    ASSERT_EQ(vdC, vdS);
}

TYPED_TEST(GraphFixture, edge_case_test4) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<edge_descriptor, bool> edge_pair = add_edge(10, vdA, g);

    ASSERT_TRUE(edge_pair.second);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 11u);

    edge_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}

TYPED_TEST(GraphFixture, edge_case_test5) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<edge_descriptor, bool> edge_pair = add_edge(vdA, 100, g);

    ASSERT_TRUE(edge_pair.second);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 101u);

    edge_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}


TYPED_TEST(GraphFixture, edge_case_test6) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<edge_descriptor, bool> edge_pair = add_edge(vdA, vdA, g);

    ASSERT_TRUE(edge_pair.second);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);

    edge_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}