// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include <unordered_set>

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = G;
    using vertex_descriptor   = typename G::vertex_descriptor;
    using edge_descriptor     = typename G::edge_descriptor;
    using vertex_iterator     = typename G::vertex_iterator;
    using edge_iterator       = typename G::edge_iterator;
    using adjacency_iterator  = typename G::adjacency_iterator;
    using vertices_size_type  = typename G::vertices_size_type;
    using edges_size_type     = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// --------
// add_edge
// --------

TYPED_TEST(GraphFixture, addedge0) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    std::pair<edge_descriptor, bool> ed = add_edge(0, 1, g);

    EXPECT_TRUE(ed.second);

    vertex_descriptor v1 = vertex(0, g);
    vertex_descriptor v2 = vertex(1, g);

    std::pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    EXPECT_TRUE(e1.second);

}

TYPED_TEST(GraphFixture, addedge1) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    std::pair<edge_descriptor, bool> p = add_edge(vdA, 1, g);

    EXPECT_TRUE(p.second);
}

TYPED_TEST(GraphFixture, addedge2) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    std::pair<edge_descriptor, bool> ed = add_edge(100, 251, g);

    EXPECT_TRUE(ed.second);

    vertex_descriptor v1 = vertex(100, g);
    vertex_descriptor v2 = vertex(251, g);

    std::pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    EXPECT_TRUE(e1.second);

}

TYPED_TEST(GraphFixture, addedge3) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    std::pair<edge_descriptor, bool> p = add_edge(vdA, -1, g);

    EXPECT_TRUE(p.second);
}

TYPED_TEST(GraphFixture, addedge4) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
    EXPECT_TRUE(e1.second);

    std::pair<edge_descriptor, bool> e2 = add_edge(v1, v2, g);
    EXPECT_FALSE(e2.second);

}

TYPED_TEST(GraphFixture, addedge5) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_edge(0, 130, g);

    ASSERT_EQ(num_vertices(g), 131);
}

TYPED_TEST(GraphFixture, addedge6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    add_edge(v1, 1, g);

    ASSERT_EQ(num_vertices(g), 2);

    std::pair<edge_descriptor, bool> e1 = edge(v1, 1, g);
    EXPECT_TRUE(e1.second);
}

// ----------
// add_vertex
// ----------

TYPED_TEST(GraphFixture, addvertex0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    std::unordered_set<vertex_descriptor> unique_vertex_descriptors;

    for (int i = 0; i < 10000; ++i) {
        vertex_descriptor vd = add_vertex(g);
        EXPECT_TRUE(unique_vertex_descriptors.insert(vd).second);
    }
}

TYPED_TEST(GraphFixture, addvertex1) {
    using graph_type          = typename TestFixture::graph_type;

    size_t expected_num_vertices = 0;
    graph_type g;
    ASSERT_EQ(num_vertices(g), expected_num_vertices);

    for (int i = 0; i < 10000; ++i) {
        add_vertex(g);
        ++expected_num_vertices;
        ASSERT_EQ(num_vertices(g), expected_num_vertices);
    }
}

// -----------------
// adjacent_vertices
// -----------------

TYPED_TEST(GraphFixture, adj0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    ASSERT_EQ(num_vertices(g), 3);

    adjacent_vertices(1000, g);

    ASSERT_EQ(num_vertices(g), 3);
}

TYPED_TEST(GraphFixture, adj1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    // vdA does not have neighbors
    ASSERT_EQ(b, e);
}

// ----
// edge
// ----

TYPED_TEST(GraphFixture, edge0) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> ed = edge(v1, v2, g);

    EXPECT_FALSE(ed.second);
}

TYPED_TEST(GraphFixture, edge1) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    add_edge(v1, v2, g);
    add_edge(v2, v1, g);

    std::pair<edge_descriptor, bool> e1 = edge(v1, v2, g);
    EXPECT_TRUE(e1.second);

    std::pair<edge_descriptor, bool> e2 = edge(v2, v1, g);
    EXPECT_TRUE(e2.second);

}

// -----
// edges
// -----

TYPED_TEST(GraphFixture, edges0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    edge_descriptor edDC = add_edge(vdD, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edCA);
    ++b;
    ASSERT_NE(e, b);

    edge_descriptor ed3 = *b;
    ASSERT_EQ(ed3, edDC);
    ++b;
    ASSERT_EQ(e, b);
}

// ---------
// num_edges
// ---------

TYPED_TEST(GraphFixture, numedges0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    add_edge(v1, v2, g);

    ASSERT_EQ(1, num_edges(g));

    add_edge(v1, v2, g);

    ASSERT_EQ(1, num_edges(g));

}

TYPED_TEST(GraphFixture, numedges1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v2, g);
    ASSERT_EQ(1, num_edges(g));

    add_edge(v1, v3, g);
    ASSERT_EQ(2, num_edges(g));

    add_edge(v2, v1, g);
    ASSERT_EQ(3, num_edges(g));

    add_edge(v2, v3, g);
    ASSERT_EQ(4, num_edges(g));

    add_edge(v3, v1, g);
    ASSERT_EQ(5, num_edges(g));

    add_edge(v3, v2, g);
    ASSERT_EQ(6, num_edges(g));

}

TYPED_TEST(GraphFixture, numedges2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_EQ(0, num_edges(g));

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);

    add_edge(v1, v1, g);
    ASSERT_EQ(1, num_edges(g));

    add_edge(v2, v2, g);
    ASSERT_EQ(2, num_edges(g));

    add_edge(v3, v3, g);
    ASSERT_EQ(3, num_edges(g));

}

// ------------
// num_vertices
// ------------

TYPED_TEST(GraphFixture, numvertices0) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;
    ASSERT_EQ(0, num_vertices(g));

    add_vertex(g);
    ASSERT_EQ(1, num_vertices(g));

    add_vertex(g);
    ASSERT_EQ(2, num_vertices(g));

}

TYPED_TEST(GraphFixture, numvertices1) {
    using graph_type          = typename TestFixture::graph_type;

    graph_type g;

    add_edge(0, 1000, g);

    ASSERT_EQ(1001, num_vertices(g));

    add_vertex(g);
    ASSERT_EQ(1002, num_vertices(g));

    add_vertex(g);
    ASSERT_EQ(1003, num_vertices(g));

}

// ------
// source
// ------

TYPED_TEST(GraphFixture, source0) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(v2, v1, g);

    EXPECT_TRUE(e1.second);
    EXPECT_TRUE(e2.second);

    ASSERT_EQ(source(e1.first, g), v1);
    ASSERT_EQ(source(e2.first, g), v2);
}

TYPED_TEST(GraphFixture, source1) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v1, g);

    EXPECT_TRUE(e1.second);

    ASSERT_EQ(source(e1.first, g), v1);
}

// ------
// target
// ------

TYPED_TEST(GraphFixture, target0) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
    std::pair<edge_descriptor, bool> e2 = add_edge(v2, v1, g);

    EXPECT_TRUE(e1.second);
    EXPECT_TRUE(e2.second);

    ASSERT_EQ(target(e1.first, g), v2);
    ASSERT_EQ(target(e2.first, g), v1);
}

TYPED_TEST(GraphFixture, target1) {
    using graph_type          = typename TestFixture::graph_type;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = add_edge(v1, v1, g);

    EXPECT_TRUE(e1.second);

    ASSERT_EQ(target(e1.first, g), v1);
}

// ------
// vertex
// ------

TYPED_TEST(GraphFixture, vertex0) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v = vertex(2, g);

    ASSERT_EQ(v, 2);

    ASSERT_EQ(num_vertices(g), 0);
}

TYPED_TEST(GraphFixture, vertex1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);

    vertex_descriptor v2 = vertex(v1, g);

    ASSERT_EQ(v1, v2);
}

// --------
// vertices
// --------

TYPED_TEST(GraphFixture, vertices0) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdB, 400, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    for (size_t i = 0; i < 401; ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, vertices1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    add_edge(vdB, 400, g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;

    for (size_t i = 0; i < 401; ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_EQ(b, e);

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    p = vertices(g);
    b = p.first;
    e = p.second;

    for (size_t i = 0; i < 404; ++i) {
        ASSERT_EQ(*b, i);
        ++b;
    }
    ASSERT_EQ(b, e);
}
