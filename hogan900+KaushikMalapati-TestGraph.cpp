// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test
{
    // ------
    // usings
    // ------

    using graph_type = G;
    using vertex_descriptor = typename G::vertex_descriptor;
    using edge_descriptor = typename G::edge_descriptor;
    using vertex_iterator = typename G::vertex_iterator;
    using edge_iterator = typename G::edge_iterator;
    using adjacency_iterator = typename G::adjacency_iterator;
    using vertices_size_type = typename G::vertices_size_type;
    using edges_size_type = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using graph_types =
    Types<
        boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>
  , Graph
        >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types, );
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first, edAB);
    ASSERT_FALSE(p1.second);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first, edAB);
    ASSERT_TRUE(p2.second);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3)
{

    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4)
{
    using graph_type		 = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

// directed graph

TYPED_TEST(GraphFixture, directed1)
{
    using graph_type 		= typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e3 = add_edge(vdC, vdD, g);
    pair<edge_descriptor, bool> e4 = add_edge(vdD, vdE, g);
    pair<edge_descriptor, bool> e5 = add_edge(vdE, vdF, g);

    vertex_descriptor s1 = source(e1.first, g);
    vertex_descriptor s2 = source(e2.first, g);
    vertex_descriptor s3 = source(e3.first, g);
    vertex_descriptor s4 = source(e4.first, g);
    vertex_descriptor s5 = source(e5.first, g);

    ASSERT_EQ(s1, 0);
    ASSERT_EQ(s2, 1);
    ASSERT_EQ(s3, 2);
    ASSERT_EQ(s4, 3);
    ASSERT_EQ(s5, 4);
}

// cyclic, sparse graph
TYPED_TEST(GraphFixture, cyclic1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e3 = add_edge(vdC, vdA, g);

    ASSERT_TRUE(e1.second);
    ASSERT_TRUE(e2.second);
    ASSERT_TRUE(e3.second);
}

// add_edge

TYPED_TEST(GraphFixture, add_edge1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e3 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e4 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e5 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e6 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e7 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e8 = add_edge(vdA, vdB, g);

    ASSERT_TRUE(e1.second);
    ASSERT_FALSE(e2.second);
    ASSERT_FALSE(e3.second);
    ASSERT_FALSE(e4.second);
    ASSERT_FALSE(e5.second);
    ASSERT_FALSE(e6.second);
    ASSERT_FALSE(e7.second);
    ASSERT_FALSE(e8.second);
}

TYPED_TEST(GraphFixture, add_edge2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> e1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = add_edge(vdA, vdB, g);

    pair<edge_descriptor, bool> e3 = add_edge(vdC, vdA, g);
    pair<edge_descriptor, bool> e4 = add_edge(vdB, vdA, g);
    pair<edge_descriptor, bool> e5 = add_edge(vdA, vdA, g);

    pair<edge_descriptor, bool> e6 = add_edge(vdA, vdC, g);
    pair<edge_descriptor, bool> e7 = add_edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e8 = add_edge(vdC, vdC, g);

    ASSERT_TRUE(e1.second);
    ASSERT_FALSE(e2.second);

    ASSERT_TRUE(e3.second);
    ASSERT_TRUE(e4.second);
    ASSERT_TRUE(e5.second);

    ASSERT_TRUE(e6.second);
    ASSERT_TRUE(e7.second);
    ASSERT_TRUE(e8.second);
}

TYPED_TEST(GraphFixture, add_edge3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    for (int i = 0; i < 10; ++i)
    {
        add_edge(vdA, vdB, g);
        add_edge(vdC, vdA, g);
    }

    ASSERT_EQ(num_edges(g), 2);
}

// add_vertex

TYPED_TEST(GraphFixture, add_vertex1)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 100; ++i)
    {
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 100);
}

TYPED_TEST(GraphFixture, add_vertex2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);
    vertex_descriptor vdH = add_vertex(g);
    vertex_descriptor vdI = add_vertex(g);

    ASSERT_EQ(vdA, 0);
    ASSERT_EQ(vdB, 1);
    ASSERT_EQ(vdC, 2);
    ASSERT_EQ(vdD, 3);
    ASSERT_EQ(vdE, 4);
    ASSERT_EQ(vdF, 5);
    ASSERT_EQ(vdG, 6);
    ASSERT_EQ(vdH, 7);
    ASSERT_EQ(vdI, 8);
    ASSERT_EQ(num_vertices(g), 9);
}

TYPED_TEST(GraphFixture, add_vertex3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    for (int i = 0; i < 10; ++i)
    {
        add_vertex(g);
    }

    add_edge(vdA, vertex(6, g), g);
    add_edge(vdA, vertex(7, g), g);
    add_edge(vdA, vertex(8, g), g);
    add_edge(vdA, vertex(9, g), g);
    add_edge(vdA, vertex(10, g), g);

    for (int i = 6; i <= 10; ++i)
    {
        ASSERT_EQ(vertex(i, g), i);
    }
    ASSERT_EQ(vdA, vertex(0, g));
    ASSERT_EQ(vdB, vertex(1, g));
    ASSERT_EQ(vdC, vertex(2, g));
    ASSERT_EQ(num_vertices(g), 13);
    ASSERT_EQ(num_edges(g), 5);
}

// adjacent_vertices

TYPED_TEST(GraphFixture, adjacent_vertices1)
{
    using graph_type = typename TestFixture::graph_type;

    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    for (int i = 0; i <= 10; ++i)
    {
        add_vertex(g);
    }

    for (int i = 1; i <= 10; ++i)
    {
        add_edge(vertex(0, g), vertex(i, g), g);
    }

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vertex(0, g), g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;
    ASSERT_NE(b, e);

    int counter = 1;
    while (b != e)
    {
        ASSERT_EQ(*b, counter);
        ++b;
        ++counter;
    }
}

TYPED_TEST(GraphFixture, adjacent_vertices2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    add_edge(vdB, vdA, g);
    add_edge(vdB, vdC, g);

    add_edge(vdC, vdA, g);
    add_edge(vdC, vdB, g);

    pair<adjacency_iterator, adjacency_iterator> p1 = adjacent_vertices(vdA, g);
    pair<adjacency_iterator, adjacency_iterator> p2 = adjacent_vertices(vdB, g);
    pair<adjacency_iterator, adjacency_iterator> p3 = adjacent_vertices(vdC, g);

    adjacency_iterator b1 = p1.first;
    adjacency_iterator e1 = p1.second;
    ASSERT_NE(b1, e1);

    adjacency_iterator b2 = p2.first;
    adjacency_iterator e2 = p2.second;
    ASSERT_NE(b2, e2);

    adjacency_iterator b3 = p3.first;
    adjacency_iterator e3 = p3.second;
    ASSERT_NE(b3, e3);

    ASSERT_EQ(*b1, 1);
    ASSERT_EQ(*b2, 0);
    ASSERT_EQ(*b3, 0);
}

TYPED_TEST(GraphFixture, adjacent_vertices3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor v = add_vertex(g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(v, g);
    adjacency_iterator b = p.first;
    adjacency_iterator e = p.second;

    ASSERT_EQ(b, e);
    ASSERT_EQ(num_vertices(g), 1);
    ASSERT_EQ(num_edges(g), 0);
}

// edge

TYPED_TEST(GraphFixture, edge1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e3 = edge(vdC, vdA, g);
    pair<edge_descriptor, bool> e4 = edge(vdA, vdA, g);

    ASSERT_FALSE(e1.second);
    ASSERT_FALSE(e2.second);
    ASSERT_FALSE(e3.second);
    ASSERT_FALSE(e4.second);
    ASSERT_EQ(num_edges(g), 0);
}

TYPED_TEST(GraphFixture, edge2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    for (int i = 0; i < 10; ++i)
    {
        add_vertex(g);
    }

    add_edge(vdA, vertex(6, g), g);
    add_edge(vdA, vertex(7, g), g);
    add_edge(vdA, vertex(8, g), g);
    add_edge(vdA, vertex(9, g), g);
    add_edge(vdA, vertex(10, g), g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vertex(6, g), g);
    pair<edge_descriptor, bool> e2 = edge(vdA, vertex(7, g), g);
    pair<edge_descriptor, bool> e3 = edge(vdA, vertex(8, g), g);
    pair<edge_descriptor, bool> e4 = edge(vdA, vertex(9, g), g);
    pair<edge_descriptor, bool> e5 = edge(vdA, vertex(10, g), g);

    ASSERT_TRUE(e1.second);
    ASSERT_TRUE(e2.second);
    ASSERT_TRUE(e3.second);
    ASSERT_TRUE(e4.second);
    ASSERT_TRUE(e5.second);
    ASSERT_EQ(vdA, vertex(0, g));
    ASSERT_EQ(vdB, vertex(1, g));
    ASSERT_EQ(vdC, vertex(2, g));
    ASSERT_EQ(num_vertices(g), 13);
    ASSERT_EQ(num_edges(g), 5);
}

TYPED_TEST(GraphFixture, edge3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdA, g);

    pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    pair<edge_descriptor, bool> e2 = edge(vdB, vdC, g);
    pair<edge_descriptor, bool> e3 = edge(vdC, vdA, g);

    pair<edge_descriptor, bool> e4 = edge(vdB, vdA, g);
    pair<edge_descriptor, bool> e5 = edge(vdB, vdB, g);
    pair<edge_descriptor, bool> e6 = edge(vdA, vdC, g);

    ASSERT_TRUE(e1.second);
    ASSERT_TRUE(e2.second);
    ASSERT_TRUE(e3.second);
    ASSERT_FALSE(e4.second);
    ASSERT_FALSE(e5.second);
    ASSERT_FALSE(e6.second);
}

// edges

TYPED_TEST(GraphFixture, edges1)
{
    using graph_type = typename TestFixture::graph_type;

    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    for (int i = 0; i < 3; ++i)
    {
        add_vertex(g);
    }

    for (int i = 0; i < 3; ++i)
    {
        add_edge(vertex(0, g), vertex(i, g), g);
    }

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    ASSERT_NE(b, e);

    for (int i = 0; i < 3; ++i)
    {
        ++b;
    }
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edges2)
{
    using graph_type = typename TestFixture::graph_type;

    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    for (int i = 0; i < 10; ++i)
    {
        add_vertex(g);
    }

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, edges3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;
    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);

    edge_descriptor e1 = add_edge(v1, v2, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;

    ASSERT_NE(b, e);
    ASSERT_EQ(*b, e1);
    ++b;
    ASSERT_EQ(b, e);
}

// num_edges

TYPED_TEST(GraphFixture, num_edges1)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 3; ++i)
    {
        add_vertex(g);
    }

    for (int i = 0; i < 3; ++i)
    {
        add_edge(vertex(0, g), vertex(i, g), g);
        add_edge(vertex(1, g), vertex(i, g), g);
        add_edge(vertex(2, g), vertex(i, g), g);
    }

    ASSERT_EQ(num_edges(g), 9);
}

TYPED_TEST(GraphFixture, num_edges2)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 7; ++i)
    {
        add_vertex(g);
    }

    for (int i = 0; i < 7; ++i)
    {
        add_edge(vertex(0, g), vertex(i, g), g);
        add_edge(vertex(1, g), vertex(i, g), g);
        add_edge(vertex(2, g), vertex(i, g), g);
        add_edge(vertex(3, g), vertex(i, g), g);
        add_edge(vertex(4, g), vertex(i, g), g);
        add_edge(vertex(5, g), vertex(i, g), g);
        add_edge(vertex(6, g), vertex(i, g), g);
    }

    ASSERT_EQ(num_edges(g), 49);
}

TYPED_TEST(GraphFixture, num_edges3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdD, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdF, g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdD, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdF, g);

    edge(vdA, vdB, g);
    edge(vdB, vdC, g);
    edge(vdC, vdD, g);
    edge(vdD, vdE, g);
    edge(vdE, vdF, g);

    ASSERT_NE(num_edges(g), 15);
    ASSERT_NE(num_edges(g), 10);
    ASSERT_EQ(num_edges(g), 5);
}

// num_vertices

TYPED_TEST(GraphFixture, num_vertices1)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    for (int i = 0; i < 78741; ++i)
    {
        add_vertex(g);
    }
    ASSERT_EQ(num_vertices(g), 78741);
}

TYPED_TEST(GraphFixture, num_vertices2)
{
    using graph_type = typename TestFixture::graph_type;

    graph_type g;

    int collatz = 10;

    while (collatz != 1)
    {
        if (collatz % 2 == 0)
        {
            collatz /= 2;
        }
        else
        {
            collatz *= 3;
            ++collatz;
        }
        add_vertex(g);
    }
    add_vertex(g);
    ASSERT_EQ(num_vertices(g), 7);
}

TYPED_TEST(GraphFixture, num_vertices3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    for (int i = 0; i < 3; ++i)
    {
        add_vertex(g);
    }

    ASSERT_EQ(num_vertices(g), 9);
}

// source

TYPED_TEST(GraphFixture, source1)
{
	using graph_type 		= typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor v1 = add_vertex(g);
	vertex_descriptor v2 = add_vertex(g);

	pair<edge_descriptor, bool> p = add_edge(v1, v2, g);

	vertex_descriptor s = source(p.first, g);

	ASSERT_EQ(s, v1);
	ASSERT_EQ(s, vertex(0, g));
}

TYPED_TEST(GraphFixture, source2)
{
	using graph_type		= typename TestFixture::graph_type;
    using vertex_descriptor	= typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor v1 = add_vertex(g);

	for (int i = 0; i < 5; ++i) {
		add_vertex(g);
	}

	pair<edge_descriptor, bool> p1 = add_edge(v1, vertex(1, g), g);
	pair<edge_descriptor, bool> p2 = add_edge(v1, vertex(2, g), g);
	pair<edge_descriptor, bool> p3 = add_edge(v1, vertex(3, g), g);
	vertex_descriptor s1 = source(p1.first, g);
	vertex_descriptor s2 = source(p2.first, g);
	vertex_descriptor s3 = source(p3.first, g);

	ASSERT_EQ(s1, v1);
	ASSERT_EQ(s2, v1);
	ASSERT_EQ(s3, v1);
}

TYPED_TEST(GraphFixture, source3)
{
	using graph_type		= typename TestFixture::graph_type;
    using vertex_descriptor	= typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor v1 = add_vertex(g);
	vertex_descriptor v2 = add_vertex(g);

	pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
	pair<edge_descriptor, bool> e2 = add_edge(v2, v1, g);
	pair<edge_descriptor, bool> e3 = add_edge(v1, v1, g);

	vertex_descriptor s1 = source(e1.first, g);
	vertex_descriptor s2 = source(e2.first, g);
	vertex_descriptor s3 = source(e3.first, g);

	ASSERT_EQ(s1, v1);
	ASSERT_EQ(s2, v2);
	ASSERT_EQ(s3, v1);
}

// target

TYPED_TEST(GraphFixture, target1)
{
	using graph_type 		= typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor v1 = add_vertex(g);
	vertex_descriptor v2 = add_vertex(g);

	pair<edge_descriptor, bool> p = add_edge(v1, v2, g);

	vertex_descriptor t = target(p.first, g);

	ASSERT_EQ(t, v2);
	ASSERT_EQ(t, vertex(1, g));
}

TYPED_TEST(GraphFixture, target2)
{
	using graph_type 		= typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor v1 = add_vertex(g);

	for (int i = 0; i < 10; ++i) {
		add_vertex(g);
	}

	pair<edge_descriptor, bool> p1 = add_edge(v1, vertex(1, g), g);
	pair<edge_descriptor, bool> p2 = add_edge(v1, vertex(2, g), g);
	pair<edge_descriptor, bool> p3 = add_edge(v1, vertex(3, g), g);
	pair<edge_descriptor, bool> p4 = add_edge(v1, vertex(4, g), g);
	pair<edge_descriptor, bool> p5 = add_edge(v1, vertex(5, g), g);
	pair<edge_descriptor, bool> p6 = add_edge(v1, vertex(6, g), g);
	pair<edge_descriptor, bool> p7 = add_edge(v1, vertex(7, g), g);
	pair<edge_descriptor, bool> p8 = add_edge(v1, vertex(8, g), g);
	pair<edge_descriptor, bool> p9 = add_edge(v1, vertex(9, g), g);
	pair<edge_descriptor, bool> p10 = add_edge(v1, vertex(10, g), g);
	
	vertex_descriptor t1 = target(p1.first, g);
	vertex_descriptor t2 = target(p2.first, g);
	vertex_descriptor t3 = target(p3.first, g);
	vertex_descriptor t4 = target(p4.first, g);
	vertex_descriptor t5 = target(p5.first, g);
	vertex_descriptor t6 = target(p6.first, g);
	vertex_descriptor t7 = target(p7.first, g);
	vertex_descriptor t8 = target(p8.first, g);
	vertex_descriptor t9 = target(p9.first, g);
	vertex_descriptor t10 = target(p10.first, g);

	ASSERT_EQ(t1, vertex(1, g));
	ASSERT_EQ(t2, vertex(2, g));
	ASSERT_EQ(t3, vertex(3, g));
	ASSERT_EQ(t4, vertex(4, g));
	ASSERT_EQ(t5, vertex(5, g));
	ASSERT_EQ(t6, vertex(6, g));
	ASSERT_EQ(t7, vertex(7, g));
	ASSERT_EQ(t8, vertex(8, g));
	ASSERT_EQ(t9, vertex(9, g));
	ASSERT_EQ(t10, vertex(10, g));
}

TYPED_TEST(GraphFixture, target3)
{
	using graph_type		= typename TestFixture::graph_type;
    using vertex_descriptor	= typename TestFixture::vertex_descriptor;
	using edge_descriptor 	= typename TestFixture::edge_descriptor;

    graph_type g;

	vertex_descriptor v1 = add_vertex(g);
	vertex_descriptor v2 = add_vertex(g);

	pair<edge_descriptor, bool> e1 = add_edge(v1, v2, g);
	pair<edge_descriptor, bool> e2 = add_edge(v2, v1, g);
	pair<edge_descriptor, bool> e3 = add_edge(v1, v1, g);

	vertex_descriptor t1 = target(e1.first, g);
	vertex_descriptor t2 = target(e2.first, g);
	vertex_descriptor t3 = target(e3.first, g);

	ASSERT_EQ(t1, v2);
	ASSERT_EQ(t2, v1);
	ASSERT_EQ(t3, v1);
}

// vertex

TYPED_TEST(GraphFixture, vertex1)
{
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    for (int i = 0; i < 10; ++i)
    {
        vertex_descriptor v = add_vertex(g);
        ASSERT_EQ(v, vertex(i, g));
    }
}

TYPED_TEST(GraphFixture, vertex2)
{
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    
    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);
    vertex_descriptor v5 = add_vertex(g);

    ASSERT_EQ(v1, 0);
    ASSERT_EQ(v2, 1);
    ASSERT_EQ(v3, 2);
    ASSERT_EQ(v4, 3);
    ASSERT_EQ(v5, 4);

}

TYPED_TEST(GraphFixture, vertex3)
{
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    
    graph_type g;

    for (int i = 0; i < 100; ++i) {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
	vertex_iterator e = p.second;

	ASSERT_NE(b, e);

	for (int i = 0; i < 100; ++i) {
        ASSERT_EQ(*b, vertex(i, g));
		++b;
    }
	ASSERT_EQ(b, e);
}

// vertices

TYPED_TEST(GraphFixture, vertices1)
{
    using graph_type = typename TestFixture::graph_type;

    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    for (int i = 0; i < 10; ++i)
    {
        add_vertex(g);
    }

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;

    ASSERT_NE(b, e);

    for (int i = 0; i < 10; ++i)
    {
        ASSERT_EQ(*b, vertex(i, g));
        ++b;
    }
}

TYPED_TEST(GraphFixture, vertices2)
{
    using graph_type = typename TestFixture::graph_type;

    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, vertices3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor v1 = add_vertex(g);
    vertex_descriptor v2 = add_vertex(g);
    vertex_descriptor v3 = add_vertex(g);
    vertex_descriptor v4 = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;

    ASSERT_NE(b, e);
    ASSERT_EQ(*b, v1);

    ++b;

    ASSERT_EQ(*b, v2);

    ++b;

    ASSERT_EQ(*b, v3);

    ++b;

    ASSERT_EQ(*b, v4);
}
