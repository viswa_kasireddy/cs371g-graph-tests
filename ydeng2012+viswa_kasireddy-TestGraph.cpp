// -------------
// TestGraph.c++
// -------------

// https://www.boost.org/doc/libs/1_73_0/libs/graph/doc/index.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// ---------
// TestGraph
// ---------

template <typename G>
struct GraphFixture : Test
{
    // ------
    // usings
    // ------

    using graph_type = G;
    using vertex_descriptor = typename G::vertex_descriptor;
    using edge_descriptor = typename G::edge_descriptor;
    using vertex_iterator = typename G::vertex_iterator;
    using edge_iterator = typename G::edge_iterator;
    using adjacency_iterator = typename G::adjacency_iterator;
    using vertices_size_type = typename G::vertices_size_type;
    using edges_size_type = typename G::edges_size_type;
};

// directed, sparse, unweighted
// possibly connected
// possibly cyclic
using graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph // uncomment
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;
    Graph my_graph;

    vertex_descriptor vd = vertex(0, g);


    vertex_descriptor vdA = add_vertex(g);


//    ASSERT_EQ(vd, vdA);
    ASSERT_EQ(vd, vdA);

    cout << "our graph" << endl;
    vertices_size_type vs = num_vertices(g);

    //  ASSERT_EQ(vs, 1u);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edges_size_type = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    //directed graph
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first, edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first, edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3)
{
    using graph_type = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor = typename TestFixture::edge_descriptor;
    using edge_iterator = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator b = p.first;
    edge_iterator e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}


TYPED_TEST(GraphFixture, edge_test1) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 1);
}

TYPED_TEST(GraphFixture, edge_test2) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    add_edge(a, b, g);

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 1);
}

TYPED_TEST(GraphFixture, edge_test3) {
    using graph_type         = typename TestFixture::graph_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    add_edge(b, a, g);

    edges_size_type size = num_edges(g);
    ASSERT_EQ(size, 2);
}

TYPED_TEST(GraphFixture, edge_test4) {

    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    pair<edge_descriptor, bool> yes = edge(a, b, g);
    pair<edge_descriptor, bool> no = edge(b, a, g);

    ASSERT_FALSE(no.second);
    ASSERT_TRUE(yes.second);

}

TYPED_TEST(GraphFixture, edge_test5) {

    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    add_edge(b, a, g);
    pair<edge_descriptor, bool> abg = edge(a, b, g);
    pair<edge_descriptor, bool> bag = edge(b, a, g);

    ASSERT_TRUE(abg.second);
    ASSERT_TRUE(bag.second);

}

TYPED_TEST(GraphFixture, edge_test6) {

    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    edge_descriptor agbed = add_edge(a, b, g).first;
    pair<edge_descriptor, bool> abg = edge(a, b, g);

    ASSERT_EQ(agbed, abg.first);

}

TYPED_TEST(GraphFixture, adjacent_vertex_1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);


    auto adj1 = adjacent_vertices(a, g).first;
    auto adj2 = adjacent_vertices(a, g).second;
    ASSERT_TRUE(adj1 == adj2);
}

TYPED_TEST(GraphFixture, adjacent_vertex_2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    add_edge(a,b,g);

    auto adj1 = adjacent_vertices(a, g).first;
    auto adj2 = adjacent_vertices(a, g).second;
    ASSERT_TRUE(adj1 != adj2);
}

TYPED_TEST(GraphFixture, vertex_misc1) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    add_edge(a,b,g);

    ASSERT_EQ(vertex(0,g),a);
}

TYPED_TEST(GraphFixture, distance) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    auto v1 = vertices(g).first;
    auto v2 = vertices(g).second;

    ASSERT_EQ(std::distance(v1,v2),1);
}

TYPED_TEST(GraphFixture, distance2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    auto v1 = vertices(g).first;
    auto v2 = vertices(g).second;
    ++v1;
    ASSERT_EQ(std::distance(v1,v2),0);
}

TYPED_TEST(GraphFixture, target) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    auto e = add_edge(a,b,g).first;

    ASSERT_EQ(target(e,g),b);
}

TYPED_TEST(GraphFixture, target2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    auto e = add_edge(a,b,g).first;

    ASSERT_NE(target(e,g),a);
}

TYPED_TEST(GraphFixture, source) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    auto e = add_edge(a,b,g).first;

    ASSERT_NE(source(e,g),b);
}

TYPED_TEST(GraphFixture, source2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    auto e = add_edge(a,b,g).first;

    ASSERT_EQ(source(e,g),a);
}

TYPED_TEST(GraphFixture, vertex_misc2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for (int i = 0; i < 10; ++i)
    {
        vertex_descriptor a = add_vertex(g);
        //vertex_descriptor b = add_vertex(g);
    }
    vertices_size_type size = num_vertices(g);


    ASSERT_EQ(size, 10);
}

TYPED_TEST(GraphFixture, vertex_misc3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertices_size_type size = num_vertices(g);


    ASSERT_EQ(size, 0);
}

TYPED_TEST(GraphFixture, edges_misc) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    graph_type g;

    edges_size_type size = num_vertices(g);


    ASSERT_EQ(size, 0);
}

TYPED_TEST(GraphFixture, edge_misc2) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    add_edge(a, b, g);


    pair<edge_iterator, edge_iterator> edgepair = edges(g);
    edgepair.first++;
    ASSERT_EQ(edgepair.first, edgepair.second);



}

TYPED_TEST(GraphFixture, edge_misc3) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);

    add_edge(a, b, g);
    add_edge(b, a, g);


    pair<edge_iterator, edge_iterator> edgepair = edges(g);
    edgepair.first++;
    ASSERT_NE(edgepair.first, edgepair.second);



}

TYPED_TEST(GraphFixture, edge_misc4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_iterator       = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    a = add_vertex(g);

    add_edge(a, a, g);


    pair<edge_iterator, edge_iterator> edgepair = edges(g);
    edgepair.first++;
    ASSERT_EQ(edgepair.first, edgepair.second);



}

TYPED_TEST(GraphFixture, vertex_misc5) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    add_edge(a, b, g);
    add_edge(a, c, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(a, g);

    int count = 0;
    while (adj.first != adj.second) {
        ++adj.first;
        ++count;
    }

    ASSERT_EQ(count, 2);
}

TYPED_TEST(GraphFixture, vertex_misc6) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    add_edge(a, b, g);
    add_edge(b, c, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(a, g);

    int count = 0;
    while (adj.first != adj.second) {
        ++adj.first;
        ++count;
    }

    ASSERT_EQ(count, 1);
}

TYPED_TEST(GraphFixture, vertex_misc7) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    add_edge(a, b, g);
    add_edge(c, b, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(a, g);

    int count = 0;
    while (adj.first != adj.second) {
        ++adj.first;
        ++count;
    }

    ASSERT_EQ(count, 1);
}

TYPED_TEST(GraphFixture, vertex_misc8) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor a = add_vertex(g);
    vertex_descriptor b = add_vertex(g);
    vertex_descriptor c = add_vertex(g);

    add_edge(a, b, g);

    pair<adjacency_iterator, adjacency_iterator> adj = adjacent_vertices(a, g);

    int count = 0;
    while (adj.first != adj.second) {
        ++adj.first;
        ++count;
    }

    ASSERT_EQ(count, 1);
}

TYPED_TEST(GraphFixture, edges_misc5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edges_size_type = typename TestFixture::edges_size_type;
    graph_type g;


    vertex_descriptor a = add_vertex(g);
    add_edge(a, a, g);
    edges_size_type size = num_vertices(g);

    ASSERT_EQ(size, 1);
}